/*
 * File Name:	BankAccountTest.java
 * Author:		George Krieger & Tyson Moyes
 * Date:		February 28th 2019
 * Purpose:		This file was (mostly) provided by the professor to test ChequingAccount and SavingsAccount, and their superclass, AbstractAccount.
*/
package bankAccountTestPkg;

import bankAccountPkg.*;

/**
 * CST8284 - ACA2
 * <P> class to launch bank account classes with test data
 * @author kriger and Tyson Moyes
 * @version 1.1
 */
public class BankAccountTest {

	/**
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		ChequingAccount myChequing = new ChequingAccount(100555, 1234567.89, 0.75);
		
		myChequing.cheque(100);
		System.out.printf("\tChequing service charges=$%3.2f\n", myChequing.getServiceCharges());
		System.out.printf("%s\n", myChequing);
		
		SavingsAccount mySavings = new SavingsAccount(901123, 1000.00, 1.34);
		mySavings.deposit(125.00);
		System.out.printf("\tSavings interim balance=$%,6.2f\n", mySavings.getBalance());
		
		mySavings.calculateMonthlyInterest();
		System.out.printf("%s\n", mySavings);
		
		AbstractAccount[] myAccounts = {myChequing, mySavings};
		
		printAccounts(myAccounts);

	} // main
	
	/**
	 * method to print out account data. uses enhanced for loop to go through accounts, and instanceof to verify what type of account it is
	 * @param accounts - bank accounts array to pass 
	 */
	private static void printAccounts(AbstractAccount[] accounts) {
		System.out.printf("\n%s%10s%25s\n", "Account type", "Balance","Charges/Interest"); 
		String str = new String();
		// loop through accounts
		for (AbstractAccount a : accounts) {
			// check if account is a chequing account or not
			if (a instanceof ChequingAccount) {
				str = String.format("%s%8s%,.2f%4s%.2f%15s", "Chequing", "$", a.getBalance(), "$", ((ChequingAccount)a).getServiceCharges(), "service charge");
			}
			else {
				str = String.format("%s%9s%,12.2f%4s%.2f%9s", "Savings","$", a.getBalance(), "$", ((SavingsAccount)a).getMonthlyInterest(), "interest");
			}
			System.out.println(str);
		}
	}
} // class
