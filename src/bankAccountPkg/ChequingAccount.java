/*
 * File Name:	ChequingAccount.Java
 * Author:		Tyson Moyes
 * Date:		February 27th 2019
 * Purpose:		Using AbstractAccount, this file creates a chequing account, with a cost per cheque and service charge values, as well as a count of how many cheques
 * 				have been created
*/
package bankAccountPkg;

/**
 * Chequing bank account, extends AbstractAccount
 * @author Tyson Moyes
 * @since JDK 1.8_181
 * @version 1.0
 */
public class ChequingAccount extends AbstractAccount {
	// Class variables
	private double costPerCheque;
	private double serviceCharges = 0.0;
	private int numCheques = 0;
	
	/**
	 * Constructor for chequing account
	 * @param accountNum - account number (int)
	 * @param initialBalance - set starting balance for account (double)
	 * @param costPerCheque - cost of cheques for account (double)
	 */
	public ChequingAccount(int accountNum, double initialBalance, double costPerCheque) {
		super(accountNum, initialBalance);
		this.costPerCheque = costPerCheque;
	}
	
	public void deposit(double amount) {
		this.balance += amount;
	}
	
	public void withdraw(double amount) {
		this.balance -= amount;
	}
	
	/**
	 * a cheque is created. A specified amount is withdrawn from the account's balance, plus the cheque charge
	 * @param amount - amount 'written' on cheque, to be withdrawn 
	 */
	public void cheque(double amount) {
		// cheque is being deposited. add 1 to numCheques
		this.numCheques++;
		withdraw(amount + this.costPerCheque);
	}
	
	/**
	 * 
	 * @return serviceCharges
	 */
	public double getServiceCharges() {
		this.serviceCharges = costPerCheque * numCheques;
		return this.serviceCharges;
	}
	

	public String toString() {
		String str = String.format("Chequing account #%d, cost per cheque=$%.2f, number of cheques=%d, balance=$%,.2f",
									this.accountNum, this.costPerCheque, this.numCheques, this.getBalance());
		return str;
	}

}
