/*
 * File Name:	SavingsAccount.java
 * Author:		Tyson Moyes
 * Date:		February 27th 2018
 * Purpose:		Using the AbstractAccount.java file, this creates a Savings account, with a yearly interest rate and monthly interest amount
*/
package bankAccountPkg;

/**
 * Savings bank account, extends AbstractAccount
 * @author Tyson Moyes
 * @since JDK 1.8_181
 * @version 1.0
 */

public class SavingsAccount extends AbstractAccount {
	private double yearlyInterestRate = 0;
	private double monthlyInterest = 0;

	/**
	 * Savings account constructor
	 * @param accountNum - account number passed to super (int)
	 * @param initialBalance - initial account balance passed to super (double)
	 * @param yearlyInterest - yearly interest, used to calculate monthly interest in calculateMonthlyInterest()
	 */
	public SavingsAccount(int accountNum, double initialBalance, double yearlyInterest) {
		super(accountNum, initialBalance);
		this.yearlyInterestRate = yearlyInterest;
	}
	
	/**
	 * Calculate monthly interest using equation provided by prof, then add it to balance
	 */
	public void calculateMonthlyInterest() {
		this.monthlyInterest = Math.pow(this.balance, this.yearlyInterestRate/12/100);
		this.balance += this.monthlyInterest;
	}
	
	public void deposit (double amount) {
		this.balance += amount;
	}
	
	public void withdraw (double amount) {
		this.balance -= amount;
	}
	
	/**
	 * getter for monthly interest
	 * @return monthlyInterest
	 */
	public double getMonthlyInterest()  {
		return this.monthlyInterest;
	}
	
	public String toString() {
		String str = String.format("Savings account #%d, yearlyInterest=%.2f%%, balance=$%,.2f",
									this.accountNum, this.yearlyInterestRate, this.balance);
		return str;
	} // toString
} // class
