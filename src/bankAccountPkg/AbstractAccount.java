/*
 * File Name:	AbstractAccount.java
 * Author:		Tyson Moyes
 * Date:		February 27th 2019
 * Purpose:		This abstract class contains the basic requirements for a bank account, including 
*/

//Packages
package bankAccountPkg;

/**
 * Abstract account class to be extended to implement specific bank account types
 * @author Tyson Moyes
 * @since JDK 1.8_181
 * @version 1.0
 */

public abstract class AbstractAccount {
	//Class variables
	protected final int accountNum;
	protected double balance;
	
	/**
	 * constructor for abstract account
	 * @param accountNum - specified int for account number
	 * @param initialBalance - specified double for initial account balance
	 */
	public AbstractAccount(int accountNum, double initialBalance) {
		// set instance variables to specified values
		this.accountNum = accountNum;
		this.balance = initialBalance;
	}
	
	/**
	 * deposit method - add specified amount to instance's account balance
	 * @param amount - specified deposit amount (double)
	 */
	public abstract void deposit(double amount);

	/**
	 * withdraw method - remove specified amount to instance's account balance
	 * @param amount - specified withdrawal amount (double)
	 */
	public abstract void withdraw(double amount);
	
	/**
	 * Getter for account balance
	 * @return account balance (double)
	 */
	public double getBalance() {
		return this.balance;
	}

	@Override
	public abstract String toString();

}
