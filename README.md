# Java Bank Account

School Assignment

This school assignment was given to us to test our knowledge of object-oriented concepts, ABSTRACT classes, and PROTECTED fields. 

The goal was to implement a class hierarchy of 3 classes that model different types of bank acocunt

**PROJECT REQUIREMENTS**
* Demonstrate OOP concepts, specifically inheritance and polymorphism
* Demonstrate proper use of abstract classes and protected fields.
* Implement an abstract account that the other classes can extend.
* Implement a chequing account class that extends the abstract account with a service charge for each cheque. The account tracks the number of checks and service charges
* Implement a savings account class that extends the abstract account with a yearly interest rate. Add the amount of interest earned monthly to the balance of the account. 
* Use provided UML diagram and Javadoc documentation to determine how to layout the program


**What I Learned**
* How to read UML diagrams (Later on, we learned how to create them as well. Love me some good UML diagrams)
* How to apply inheritance concept using abstract classes


**How To Run**

Open the project using a Java IDE like Eclipse, open the "BankAccountTest.java" file and run it.